package br.edu.fas;

import br.edu.fas.persistence.Dao;
import br.edu.fas.service.Service;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;
import static com.tngtech.archunit.library.Architectures.layeredArchitecture;
import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices;

public class FooArchitectTest {

    JavaClasses importedClasses = new ClassFileImporter().importPackages("br.edu.fas");

    @Test
    public void verificarDependenciasParaCamadaPersistencia() {

        ArchRule rule = classes()
                .that().resideInAPackage("..persistence..")
                .should().onlyHaveDependentClassesThat().resideInAnyPackage("..persistence..", "..service..");

        rule.check(importedClasses);

    }

    @Test
    public void verificarDependenciasDaCamadaPersistencia() {

        ArchRule rule = noClasses()
                .that().resideInAPackage("..persistence..")
                .should().dependOnClassesThat().resideInAnyPackage("..service..");

        rule.check(importedClasses);

    }

    @Test
    public void verificarNomesClassesCamadaPersistencia() {

        ArchRule rule = classes()
                .that().haveSimpleNameEndingWith("Dao")
                .should().resideInAPackage("..persistence..");

        rule.check(importedClasses);

    }

    @Test
    public void verificarImplementacaoInterfaceDao() {

        ArchRule rule = classes()
                .that().implement(Dao.class)
                .should().haveSimpleNameEndingWith("Dao");

        rule.check(importedClasses);

    }

    @Test
    public void verificarDependenciasCiclicas() {

        ArchRule rule = slices()
                .matching("br.edu.fas.(*)..").should().beFreeOfCycles();

        rule.check(importedClasses);

    }

    @Test
    public void verificarViolacaoCamadas() {

        ArchRule rule = layeredArchitecture()
                .layer("Service").definedBy("..service..")
                .layer("Persistence").definedBy("..persistence..")
                .layer("Controller").definedBy("..controller..")

                .whereLayer("Persistence").mayOnlyBeAccessedByLayers("Service")

                // adicionando regra que a camada de servico so pode ser acessada pela de controladores
                .whereLayer("Service").mayOnlyBeAccessedByLayers("Service", "Controller");

        rule.check(importedClasses);

    }

    /**
     * Verifica se as classes que possuem dependencia para a camada de servico
     * sao apenas a propria camada de servico e os controladores
     */
    @Test
    public void verificarDependenciasParaCamadaServico() {

        ArchRule rule = classes()
                .that().resideInAPackage("..service..")
                .should().onlyHaveDependentClassesThat().resideInAnyPackage("..service..", "..controller..");

        rule.check(importedClasses);
    }

    /**
     * Testar se a camada de serviço possui apenas acesso
     * a pacotes de negocio, servico ou persistencia. O intuito eh
     * nao permitir que a camada de servico possa ter uma instancia
     * de um controlador por exemplo
     */
    @Test
    public void verificarDependenciasDaCamadaServico() {

        ArchRule rule = classes()
                .that().resideInAPackage("..service..")
                .should().onlyDependOnClassesThat().resideInAnyPackage("java..", "..service..", "..persistence..", "..business..");

        rule.check(importedClasses);
    }

    /**
     * Verificar se as classes que usam o sufixo 'Service'
     * em seus nomes estao na camada de servico
     */
    @Test
    public void verificarNomesClassesCamadaServico() {

        ArchRule rule = classes()
                .that().haveSimpleNameEndingWith("Service")
                .should().resideInAPackage("..service..");

        rule.check(importedClasses);

    }

    /**
     * Verificar se as classes de servico implementam
     * a interface {@link Service}
     */
    @Test
    public void verificarImplementacaoInterfaceService() {

        ArchRule rule = classes()
                .that().implement(Service.class)
                .should().haveSimpleNameEndingWith("Service");

        rule.check(importedClasses);

    }

    /**
     * Testar se a camada de negocio possui apenas acesso
     * a seu proprio pacote. O intuito eh
     * nao permitir que a camada de negocio possa ter uma instancia
     * de um servico por exemplo
     */
    @Test
    public void verificarDependenciasDaCamadaNegocio() {

        ArchRule rule = classes()
                .that().resideInAPackage("..business..")
                .should().onlyDependOnClassesThat().resideInAnyPackage("java..", "..business..");

        rule.check(importedClasses);
    }

}
